<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth'])->group(function () {
    // CRUD Postingan
    Route::resource('postingan', 'PostinganController');

    // CRUD Update Profile
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);

    // CRUD Update Komentar
    Route::resource('komentar', 'KomentarController')->only([
        'store'
    ]);

});


Route::get('master', function () {
    return view('layout.master');
});

Route::get('register', function () {
    return view('register');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {

    // CRUD Followers
    
    Route::get('/user/{user}','ProfileController@profile')->name('profile');
    
    Route::post('/search','SearchController@search')->name('search');
    
    Route::get('/users','FollowingController@allUsers');
    
    Route::get('/following/{id}','FollowingController@following')->name('following');
    
    Route::get('/notification','FollowingController@notification')->name('notification');
    
    Route::get('/accept/{id}','FollowingController@accept')->name('accept');
    
    Route::get('/reject/{id}','FollowingController@reject')->name('reject');
    
    Route::get('/following','FollowingController@followingUser')->name('followingUser');
    
    Route::get('/follower','FollowingController@followerUser')->name('followerUser');

});


