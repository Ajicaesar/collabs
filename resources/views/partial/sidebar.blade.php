<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="/postingan">
              <i class="icon-grid menu-icon"></i>
              <span class="menu-title">Beranda</span>
            </a>
          </li>
          
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="icon-head menu-icon"></i>
              <span class="menu-title">Profil</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="/profile"> Update Profil </a></li>
                <li class="nav-item"> <a class="nav-link" href="/users"> Cari Teman </a></li>
                <li class="nav-item"> <a class="nav-link" href="/notification"> Notifikasi </a></li>
                <li class="nav-item"> <a class="nav-link" href="{{route('profile', Auth::user()->name)}}"> Profil </a></li>
                <li class="nav-item"> <a class="nav-link" href="/following"> Teman </a></li>
                @guest
                <li class="nav-item"> <a class="nav-link" href="/login"> Login </a></li>
                <li class="nav-item"> <a class="nav-link" href="/register"> Register </a></li>
                @endguest

                @auth    
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                          <p>Logout</p>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                </li>
                @endauth
              </ul>
            </div>
          </li>
          
        </ul>
      </nav>