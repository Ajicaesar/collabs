@extends('layout.master')

@section('content')
<div class="container-fluid">
    @foreach ($user as $item)
        <div class="jumbotron">
        <img src="{{asset('images/user.jpg')}}" alt="gambar">
                <h2>{{$item->name}}</h2>
                <a href="" class="btn btn-success btn-sm">Follower ( 1k )</a>
                <a href="" class="btn btn-success btn-sm">Following ( 2k )</a>
            </div>
    @endforeach
</div>

@endsection

<style type="text/css">
    .jumbotron{
        background: url(../images/bg.jpg) left center;
    }

    .jumbotron img{
        width: 200px;
        border-radius: 50%;
        height: 200px;
        border : 2px solid #ffffff ;

    }

    .jumbotron h2{
        color: #ffffff;
        margin-bottom: 0;
        margin-left: 60px;
        margin-top: 20px;
    }
</style>