@extends('layout.master')

@section('content')

<div class="container">

    @include('partial.detail')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Follower saya ( {{$followerUser->count()}} )</h4></div>
                <div class="panel-body" style="">
                    
                    @foreach ($followerUser as $follower)
                    
                    <div class="col-md-4">
                        <div class="box-card my-5">
                            <img src="{{asset('images/user.jpg')}}" style="width: 100%">
                                <div class="panel-footer">
                                    <div class="profile-result">
                                        <img src="{{asset('images/bg.jpg')}}" alt="background">
                                    </div>
                                </div>
                                    <span>
                                        <h4 class="card-title" style="margin-bottom: 0;" ><b>{{$follower->name}}</b></h4>
                                        <p><a>@</a>{{$follower->name}}</p>
                                    </span>
                                    <a href="" class="btn btn-danger btn-sm">Remove</a>
                        </div>
                    </div>     
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection

<style type="text/css">
    .profile-result img{
        width: 27%;
        border-radius: 50%;
        height: 51px;
        border: 2px solid #ffffff;
    }

    .profile-result{
        margin-top: -38px;
    }
    .box-card{
        box-shadow: 0 1px 4px #888888;
    }

</style>