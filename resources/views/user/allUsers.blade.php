@extends('layout.master')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Total Users ({{$allUsers->count()}})</h4></div>
                <div class="panel-body" style="">
                    
                    @foreach ($allUsers as $user)
                    
                    <div class="col-md-4">
                        <div class="box-card my-5">
                            <img src="{{asset('images/user.jpg')}}" style="width: 100%">
                                <div class="panel-footer">
                                    <div class="profile-result">
                                        <img src="{{asset('images/bg.jpg')}}" alt="background">
                                    </div>
                                </div>
                                    <span>
                                        <h4 class="card-title" style="margin-bottom: 0;" ><b>{{$user->name}}</b></h4>
                                        <p><a>@</a>{{$user->name}}</p>
                                    </span>

                                <?php
                                    $if_null = App\Follower::where('follow_id','=', $user->id)->first();

                                    if(is_null($if_null)){
                                        ?>
                                        <a href="{{route('following', $user->id)}}" class="btn btn-success">Follow</a>
                                    <?php }
                                    else {?>

                                    <a href="" class="btn btn-success">Following</a>
                                        <?php }?>
                        </div>
                    </div>     
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection

<style type="text/css">
    .profile-result img{
        width: 27%;
        border-radius: 50%;
        height: 51px;
        border: 2px solid #ffffff;
    }

    .profile-result{
        margin-top: -38px;
    }
    .box-card{
        box-shadow: 0 1px 4px #888888;
    }

</style>