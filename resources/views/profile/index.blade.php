@extends('layout.master')

@section('content')

<div class="card mb-3 col-6">
    <form action="/profile/{{$profile->id}}" method="POST" class="forms my-3 mx-3" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group mx-3 mt-2">
            <div class="form-group">
                <label>Nama</label>
                    <input type="text" name="name" class="form-control p-input" value="{{$profile->user->name}}" disabled>
            </div>

            <div class="form-group">
                <label>Email Saya</label>
                    <input type="email" name="email" class="form-control p-input" value="{{$profile->user->email}}" disabled>
            </div>
            
                <label>Update Biodata</label>
                    <textarea name="biodata" class="form-control p-input" rows="4" value="">{{$profile->biodata}}</textarea>
                    @error('biodata')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                    @enderror
        </div>
        <div class="input-group mb-3 col-3">
            <button type="submit" class="btn btn-success ml-3">Update</button>
        </div>
        @error('gambar')
                    <div class="alert alert-danger ml-3">
                        {{ $message }}
                    </div>
            @enderror
    </form>
</div>


@endsection