@extends('layout.master')

@section('content')

<div class="card mb-3 col-6">
    <div class="card-body">
        <p class="mb-2 text-muted"></p>

        <h5 class="card-text">
            {{$postingan->tulisan}}
        </h5>
        <img class="card-img" src="{{asset('images/'.$postingan->gambar)}}" alt="">
    </div>
</div>

    <form action="/komentar" method="POST">
        @csrf
        <div class="form-group ">
            <h4 >Jumlah Komentar ( {{$postingan->komentar->count()}} )</h4>
            <label >Beri Komentar</p></label>
            <input type="hidden" value="{{$postingan->id}}" name="postingan_id">
            <textarea name="isi" class="form-control p-input" rows="3"></textarea>
            @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
            @enderror
            <button type="submit" class="btn btn-success ml-3">Submit</button>
    </form>

    @forelse ($postingan->komentar as $item)
                    <div class="d-flex justify-content-between align-items-center">     
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="mr-2 mt-1">
                                <img class="rounded-circle ml-4"  width="30" src="https://picsum.photos/50/50" alt="">
                            </div>
                            <div>
                                <div class="h7 text"><strong>{{$item->user->name}}</strong></div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-body">
                        <p>{{$item->isi}}</p>
                        </div>
                @empty
                    <h4> tidak ada komentar </h4>
                @endforelse

@endsection