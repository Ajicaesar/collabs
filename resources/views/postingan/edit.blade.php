@extends('layout.master')

@section('content')

@section('content')
    <form action="/postingan/{{$postingan->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Edit Postingan</label>
            <textarea name="tulisan" class="form-control" placeholder="Masukkan bio" cols="30" rows="10">{{$postingan->tulisan}}</textarea>
            @error('tulisan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection