@forelse ($postingan as $item)
<div class="card mb-3 col-6">
    <div>
        <div class="d-flex justify-content-between align-items-center">
            <div class="d-flex justify-content-between align-items-center">
                <div class="mr-2">
                    <img class="rounded-circle ml-4"  width="30" src="https://picsum.photos/50/50" alt="">
                </div>
                <div class="ml-2">
                    <div class="h7 text"><strong>{{ Auth::user()->name }}</strong></div>
                </div>
            </div>
            <div>
                <div class="dropdown">
                    <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                    <form action="/postingan/{{$item->id}}" method="POST">
                        @csrf
                                
                        @method('DELETE')
                        <a class="dropdown-item" href="/postingan/{{$item->id}}/edit">Edit</a>
                        <input type="submit" class="dropdown-item" onclick="return confirm('Apakah anda yakin?')" value="Delete">
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <p class="mb-2 text-muted">{{$item->created_at}}</p>

        <h5 class="card-text">
            {{$item->tulisan}}
        </h5>
        <img class="card-img" src="{{asset('images/'.$item->gambar)}}" alt="">
    </div>
    <div class="card-footer bg-transparent border-success">
        <a href="#" class="card-link"><i class="fa fa-gittip"></i> Like</a>
        <a class="card-link btn-transparent" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Comment </a>
    </div>
    
            <div class="collapse mx-2" id="collapseExample">
                <div class="card mb-1 md-3">
                    <form action="/postingan/{{$item->id}}" method="POST">
                        @csrf
                        <a href="/postingan/{{$item->id}}" class="btn btn-info">Berikan Komentar</a>
                    </form>
                </div>
                
                
            </div>
</div>
@empty
<p>No postingan so far!</p>
@endforelse 