@extends('layout.master')

@section('content')

<div class="card mb-3">
    <form action="/postingan" method="POST" class="forms my-3 mx-3" enctype="multipart/form-data">
        @csrf
    
        <div class="form-group mx-3">
            <label for="exampleTextarea">Post Status</label>
            <textarea name="tulisan" class="form-control p-input" id="exampleTextarea" rows="3" placeholder="Apa yang anda pikirkan"></textarea>
            @error('tulisan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
            @enderror
        </div>
        <div class="input-group mb-3 col-3">
            <div class="custom-file">
              <input type="file" class="custom-file-input" name="gambar" >
              <label class="custom-file-label">Pilih Gambar</label>
            </div>
            <button type="submit" class="btn btn-success ml-3">Submit</button>
        </div>
        @error('gambar')
                    <div class="alert alert-danger ml-3">
                        {{ $message }}
                    </div>
            @enderror
    </form>
</div>

<!--- Postingan-->
@include('postingan.index')
<!-- End Post -->


@endsection