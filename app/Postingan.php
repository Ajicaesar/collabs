<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postingan extends Model
{
    protected $table = 'postingan';
    protected $fillable = ['tulisan', 'gambar'];

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }
}
