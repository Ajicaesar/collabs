<?php

namespace App\Http\Controllers;

use App\Postingan;
use App\Komentar;
use Illuminate\Http\Request;
use DB;

class PostinganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postingan = Postingan::all();

        return view('postingan.create', compact('postingan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('postingan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tulisan' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $namaGambar = time().'.'.$request->gambar->extension();  
   
        $request->gambar->move(public_path('images'), $namaGambar);

        $postingan = new postingan;
        $postingan->tulisan = $request->tulisan;
        $postingan->gambar = $namaGambar;
        $postingan->save();

        return redirect('/postingan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Postingan  $postingan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $postingan = Postingan::FindOrFail($id);

        return view('postingan.show', compact('postingan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Postingan  $postingan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $postingan = Postingan::FindOrFail($id);

        return view('postingan.edit', compact('postingan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Postingan  $postingan
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $request->validate([
            'tulisan' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $namaGambar = time().'.'.$request->gambar->extension();  
   
        $request->gambar->move(public_path('images'), $namaGambar);

        $postingan = Postingan::find($id);
        $postingan->tulisan = $request->tulisan;
        $postingan->gambar = $namaGambar;

        $postingan->save();

        return redirect('/postingan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Postingan  $postingan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('postingan')->where('id', $id)->delete();

        return redirect('/postingan');
    }
}
