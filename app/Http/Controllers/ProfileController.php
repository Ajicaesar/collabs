<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\User;
use DB;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile(User $user)
    {
        // dd($user);

        $user = User::find($user);

        return view('user.profile', compact('user'));
    }

    public function index(){
        $profile = Profile::where('user_id', Auth::id())->first();

        return view ('profile.index', compact('profile'));
    }

    public function update($id, Request $request){
        $request->validate([
            'biodata' => 'required',
        ]);

        $profile = Profile::find($id);

        $profile->biodata = $request['biodata'];

        $profile->save();

        return redirect('/profile');
    }
}
