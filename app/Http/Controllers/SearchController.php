<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class SearchController extends Controller
{
    public function search(Request $request){
        $searchInput = $request->input('search');

        // dd($searchInput);

        $searchUsers = User::where('name','like','%'.$searchInput. '%')->get();

        return view('search.result', compact('searchUsers'));
    }
}
