<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Komentar;
use Auth;

class KomentarController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'isi' => 'required',
        ]);

        $komentar = new Komentar;
        $komentar->isi = $request->isi;
        $komentar->user_id = Auth::id();
        $komentar->postingan_id = $request->postingan_id;
        
        $komentar->save();

        return redirect()->back();
    }

}
